<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Filtros</title>
  </head>
  <body>
    <div class="container">

    <form method="Post" action="crawler.php">
  <div class="form-group">
    <label for="exampleSelect1">MARCA</label>
    <select class="form-control" id="exampleSelect1" name="marca">
      <!--<option>BMW</option>
      <option>Chevrolet</option>-->
      <option>Volkswagen</option>
    </select>
    <hr>    

    <label for="exampleSelect1">MODELO</label>
    <select class="form-control" id="exampleSelect2" name="modelo">
      <!--<option id="0001" value="1756">116i</option>
      <option id="0002" value="0000">Eurovan</option>-->
      <option id="0002" value="444">Gol</option>
    </select>
    <hr>

    <label for="exampleSelect1">CIDADE</label>
    <select class="form-control" id="exampleSelect3" name="cidade">
      <option value="2700">Belo Horizonte</option>
      <!--<option value="0000">Contagem</option>-->
    </select>
    <hr>

    <label for="exampleSelect1">PREÇO</label>
    <select class="form-control" id="valor1" name="valor1">
      <option id="" value="2000">2.000,00</option>
      <option id="" value="4000">4.000,00</option>
      <option id="" value="6000">6.000,00</option>
      <option id="" value="20000">20.000,00</option>
      <option id="" value="60000">60.000,00</option>
    </select>
    <hr>

    <label for="exampleSelect1">PREÇO até</label>
    <select class="form-control" id="valor2" name="valor2">
      <option id="" value="2000">2.000,00</option>
      <option id="" value="4000">4.000,00</option>
      <option id="" value="6000">6.000,00</option>
      <option id="" value="20000">20.000,00</option>
      <option id="" value="60000">60.000,00</option>
    </select>
    <hr>

    <label for="exampleSelect1">ANO</label>
    <select class="form-control" id="exampleSelect5" name="ano1">
      <option>2018</option>
      <option>2017</option>
      <option>2016</option>
      <option>2015</option>
      <option>2014</option>
      <option>2013</option>
    </select>
    <hr>

     <label for="exampleSelect1">ANO Até</label>
    <select class="form-control" id="exampleSelect5" name="ano2">
      <option>2018</option>
      <option>2017</option>
      <option>2016</option>
      <option>2015</option>
      <option>2014</option>
      <option>2013</option>
    </select>
    <hr>


    <div class="form-check">
    <label class="form-check-label">
        <li style="list-style: none;"><input type="radio" class="form-check-input" name="usuario" id="usuario1" value="particular" >Particular</label></li>
        <li style="list-style: none;"><input type="radio" class="form-check-input" name="usuario" id="usuario2" value="revenda" checked="">Revenda</label></li>
    </div>
    <hr>

  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
      
    </div><!--CONTAINER-->
  

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>